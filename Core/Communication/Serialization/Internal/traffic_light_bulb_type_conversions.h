/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H

#include "Core/Environment/Map/GtGenMap/Internal/traffic_light.h"
#include "gtgen_map.pb.h"

namespace gtgen::core::communication
{

inline messages::map::TrafficLightColor GtGenToProtoOsiTrafficLightColor(
    environment::map::OsiTrafficLightColor gtgen_colour_type)
{
    using gtgen_type = environment::map::OsiTrafficLightColor;
    using proto_type = messages::map::TrafficLightColor;

    switch (gtgen_colour_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_OTHER;
        }
        case gtgen_type::kRed:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_RED;
        }
        case gtgen_type::kYellow:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_YELLOW;
        }
        case gtgen_type::kGreen:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_GREEN;
        }
        case gtgen_type::kBlue:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_BLUE;
        }
        case gtgen_type::kWhite:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_WHITE;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::TrafficLightIcon GtGenToProtoOsiTrafficLightIcon(
    environment::map::OsiTrafficLightIcon gtgen_icon_type)
{
    using gtgen_type = environment::map::OsiTrafficLightIcon;
    using proto_type = messages::map::TrafficLightIcon;

    switch (gtgen_icon_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_OTHER;
        }
        case gtgen_type::kNone:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_NONE;
        }
        case gtgen_type::kArrowStraightAhead:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEAD;
        }
        case gtgen_type::kArrowLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWLEFT;
        }
        case gtgen_type::kArrowDiagonalLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDIAGONALLEFT;
        }
        case gtgen_type::kArrowStraightAheadLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADLEFT;
        }
        case gtgen_type::kArrowRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWRIGHT;
        }
        case gtgen_type::kArrowDiagonalRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDIAGONALRIGHT;
        }
        case gtgen_type::kArrowStraightAheadRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADRIGHT;
        }
        case gtgen_type::kArrowLeftRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWLEFTRIGHT;
        }
        case gtgen_type::kArrowDown:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWN;
        }
        case gtgen_type::kArrowDownLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWNLEFT;
        }
        case gtgen_type::kArrowDownRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWNRIGHT;
        }
        case gtgen_type::kArrowCross:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWCROSS;
        }
        case gtgen_type::kPedestrian:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_PEDESTRIAN;
        }
        case gtgen_type::kWalk:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_WALK;
        }
        case gtgen_type::kDontWalk:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_DONTWALK;
        }
        case gtgen_type::kBicycle:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BICYCLE;
        }
        case gtgen_type::kPedestrianAndBicycle:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_PEDESTRIANANDBICYCLE;
        }
        case gtgen_type::kCountdownSeconds:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_COUNTDOWNSECONDS;
        }
        case gtgen_type::kCountdownPercent:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_COUNTDOWNPERCENT;
        }
        case gtgen_type::kTram:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_TRAM;
        }
        case gtgen_type::kBus:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BUS;
        }
        case gtgen_type::kBusAndTram:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BUSANDTRAM;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_UNKNOWN;
        }
    }
}

inline messages::map::TrafficLightMode GtGenToProtoOsiTrafficLightMode(
    environment::map::OsiTrafficLightMode gtgen_mode_type)
{
    using gtgen_type = environment::map::OsiTrafficLightMode;
    using proto_type = messages::map::TrafficLightMode;

    switch (gtgen_mode_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_OTHER;
        }
        case gtgen_type::kOff:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_OFF;
        }
        case gtgen_type::kConstant:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_CONSTANT;
        }
        case gtgen_type::kFlashing:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_FLASHING;
        }
        case gtgen_type::kCounting:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_COUNTING;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_UNKNOWN;
        }
    }
}

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H
