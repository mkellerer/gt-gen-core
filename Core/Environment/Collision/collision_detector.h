/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_COLLISION_COLLISION_DETECTOR_H
#define GTGEN_CORE_ENVIRONMENT_COLLISION_COLLISION_DETECTOR_H

#include "Core/Environment/DataStore/output_generator.h"
#include "Core/Environment/Entities/vehicle_entity.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometry.hpp>

#include <vector>

namespace bg = boost::geometry;
typedef bg::model::d2::point_xy<double> point_t;
typedef bg::model::ring<point_t> ring_t;

using gtgen::core::environment::datastore::Acyclic;
using gtgen::core::environment::datastore::OutputGenerator;

namespace gtgen::core::environment::collision
{
/// Info about who if and how collided
struct CollisionInfo
{
    /// true if agent to agent collision, false if agent to object collision
    bool collision_with_vehicle;
    /// primary collision partner (always an agent)
    mantle_api::UniqueId collision_entity_id;
    /// secondary collision partner (could be also static object)
    mantle_api::UniqueId collision_opponent_id;
};

/// @brief   This class detectes whether a collision happen in the simulation.
/// @details This class detects whether a vehicle collided with either another vehicle or a traffic object.
class CollisionDetector
{
  public:
    /**
     * @brief CollisionDetector constructor
     * @param entity_repository Pointer to entity repository
     * @param output_generator  Data buffer from where the cyclics are retrieved each simulation step.
     */
    CollisionDetector(mantle_api::IEntityRepository* entity_repository, OutputGenerator* output_generator);
    ~CollisionDetector() = default;
    void Step(mantle_api::Time current_simulation_time);

  private:
    /// @brief Calculates distance between rings along the prefered axis
    /// @param own_ring   the first ring
    /// @param other_ring the second ring
    /// @param axis       prefered axis X or Y
    /// @return           calculated distance
    template <std::size_t axis>
    double GetCartesianDistanceAlongAxis(const ring_t& own_ring, const ring_t& other_ring) const
    {
        auto comparision = [&](const auto& a, const auto& b) { return bg::get<axis>(a) < bg::get<axis>(b); };

        const auto [own_min_element, own_max_element] =
            std::minmax_element(own_ring.begin(), own_ring.end(), comparision);
        const double own_min{bg::get<axis>(*own_min_element)};
        const double own_max{bg::get<axis>(*own_max_element)};

        const auto [other_min_element, other_max_element] =
            std::minmax_element(other_ring.begin(), other_ring.end(), comparision);
        const double other_min{bg::get<axis>(*other_min_element)};
        const double other_max{bg::get<axis>(*other_max_element)};

        double net_distance{0.0};

        if (own_max < other_min)
        {
            net_distance = other_min - own_max;
        }
        else if (own_min > other_max)
        {
            net_distance = other_max - own_min;
        }

        return net_distance;
    }

    /// @brief  Check For Collision between worldObjects
    /// @param  other_entity    pointer to another entity
    /// @param  entity          pointer to entity
    /// @return true when collision detected
    bool DetectCollision(mantle_api::IEntity* other_entity, mantle_api::IEntity* entity) const;

    /// @brief This function is responsible for updating, adding and publishing collisions
    void UpdateCollisions();

    /// @brief This function updates collision partners of the entity and its opponent. Additionally updating
    /// recursively of all existing partners of both.
    /// @param  entity   a pointer to entity object
    /// @param  opponent a pointer to opponent object
    void UpdateCollision(gtgen::core::environment::entities::BaseEntity* entity,
                         gtgen::core::environment::entities::BaseEntity* opponent);

    /// @brief IsEntityCollidable function determines whether the entity is collidable or not by
    /// checking all dimensions of the entity
    /// @param  entity pointer to an entity
    /// @return true if the entity is collidable
    bool IsEntityCollidable(const mantle_api::IEntity* entity) const;

    /// @brief Calculates bounding box points of the entity
    /// @param entitys pointer to an entity
    /// @return Points of entity's bounding box
    ring_t CalculateBoundingBox(const mantle_api::IEntity* entity) const;

    /// Publish information about the crash (e.g. for simulation output)
    /// @param collisionInfo     collision info
    void PublishCrash(const CollisionInfo& collisionInfo);

    mantle_api::IEntityRepository* entity_repository_;
    OutputGenerator* output_generator_;
    std::vector<CollisionInfo> collisions_;
};
}  // namespace gtgen::core::environment::collision

#endif  // GTGEN_CORE_ENVIRONMENT_COLLISION_COLLISION_DETECTOR_H