/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LIGHTSTATECONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LIGHTSTATECONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"

#include <MantleAPI/Traffic/control_strategy.h>

namespace gtgen::core::environment::controller
{

class IndicatorControlUnit : public IAbstractControlUnit
{
  public:
    IndicatorControlUnit(const mantle_api::VehicleLightStatesControlStrategy& control_strategy);
    IndicatorControlUnit(IndicatorControlUnit const&) = default;
    IndicatorControlUnit& operator=(const IndicatorControlUnit&) = delete;
    IndicatorControlUnit(IndicatorControlUnit&&) = delete;
    IndicatorControlUnit& operator=(IndicatorControlUnit&&) = delete;
    ~IndicatorControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;
    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    mantle_api::VehicleLightStatesControlStrategy control_strategy_;
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LIGHTSTATECONTROLUNIT_H
