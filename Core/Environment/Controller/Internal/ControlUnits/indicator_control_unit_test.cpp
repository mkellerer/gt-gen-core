/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/indicator_control_unit.h"

#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/vehicle_light_properties.h>
#include <gtest/gtest.h>

#include <memory>
#include <tuple>

namespace gtgen::core::environment::controller
{

class IndicatorControlUnitTest : public ::testing::Test
{
  public:
    IndicatorControlUnitTest()
    {
        entity_ = std::make_unique<entities::VehicleEntity>(100, "vehicle");
        entity_->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    }

  protected:
    mantle_api::VehicleLightStatesControlStrategy control_strategy_;
    std::unique_ptr<entities::VehicleEntity> entity_{};
    std::unique_ptr<IndicatorControlUnit> light_state_control_unit_;
};

TEST_F(IndicatorControlUnitTest, GivenValidVehicleLightStatesControlStrategy_WhenClone_ThenCopyIsCreated)
{
    std::unique_ptr<IndicatorControlUnit> light_state_control_unit_ =
        std::make_unique<IndicatorControlUnit>(control_strategy_);

    auto light_state_control_unit_cloned = light_state_control_unit_->Clone();

    ASSERT_NE(light_state_control_unit_cloned, nullptr);
    EXPECT_NE(light_state_control_unit_, light_state_control_unit_cloned);
}

TEST_F(IndicatorControlUnitTest, GivenValidVehicleLightStatesControlStrategy_WhenHasFinished_ThenReturnsTrue)
{
    light_state_control_unit_ = std::make_unique<IndicatorControlUnit>(control_strategy_);
    EXPECT_TRUE(light_state_control_unit_->HasFinished());
}

class IndicatorControlUnitParamTest
    : public ::testing::WithParamInterface<std::tuple<mantle_api::VehicleLightType, mantle_api::IndicatorState>>,
      public IndicatorControlUnitTest
{
};

INSTANTIATE_TEST_SUITE_P(IndicatorControlUnitInstParamTest,
                         IndicatorControlUnitParamTest,
                         ::testing::Values(
                             std::tuple<mantle_api::VehicleLightType, mantle_api::IndicatorState>{
                                 mantle_api::VehicleLightType::kIndicatorLeft,
                                 mantle_api::IndicatorState::kLeft},
                             std::tuple<mantle_api::VehicleLightType, mantle_api::IndicatorState>{
                                 mantle_api::VehicleLightType::kIndicatorRight,
                                 mantle_api::IndicatorState::kRight}));

TEST_P(IndicatorControlUnitParamTest,
       GivenVehicleLightTypeAndLightModeOnAndEntityWithIndicatorStateOff_WhenStep_ThenEntityIndicatorStateIsOn)
{
    entity_->SetIndicatorState(mantle_api::IndicatorState::kOff);

    control_strategy_.light_type = std::get<0>(GetParam());
    control_strategy_.light_state.light_mode = mantle_api::LightMode::kOn;

    light_state_control_unit_ = std::make_unique<IndicatorControlUnit>(control_strategy_);
    light_state_control_unit_->SetEntity(*entity_);

    ASSERT_NO_THROW(light_state_control_unit_->Step(mantle_api::Time(0)));
    EXPECT_EQ(entity_->GetIndicatorState(), std::get<1>(GetParam()));
}

TEST_P(IndicatorControlUnitParamTest,
       GivenVehicleLightTypeAndLightModeOffAndEntityWithIndicatorStateOn_WhenStep_ThenEntityIndicatorStateIsOff)
{
    // Switch on indicator by setting its state to left or right
    entity_->SetIndicatorState(std::get<1>(GetParam()));

    control_strategy_.light_type = std::get<0>(GetParam());
    control_strategy_.light_state.light_mode = mantle_api::LightMode::kOff;

    light_state_control_unit_ = std::make_unique<IndicatorControlUnit>(control_strategy_);
    light_state_control_unit_->SetEntity(*entity_);

    ASSERT_NO_THROW(light_state_control_unit_->Step(mantle_api::Time(0)));
    EXPECT_EQ(entity_->GetIndicatorState(), mantle_api::IndicatorState::kOff);
}

class IndicatorControlUnitInvalidLightTypeParamTest
    : public ::testing::WithParamInterface<std::tuple<mantle_api::VehicleLightType, mantle_api::IndicatorState>>,
      public IndicatorControlUnitTest
{
};

}  // namespace gtgen::core::environment::controller
