/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_BASE_FIXTURE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_BASE_FIXTURE_H

#include "Core/Environment/GtGenEnvironment/entity_repository.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::control_unit::test
{

using units::literals::operator""_m;
using units::literals::operator""_ms;
using units::literals::operator""_mps;
using units::literals::operator""_rad;

class TrafficParticipantControlUnitBaseFixture : public testing::Test
{
  protected:
    void SetUp() override
    {
        gtgen_map_ = test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints();
        traffic_command_builder_ = std::make_unique<traffic_command::TrafficCommandBuilder>();
    }

    controller::TrafficParticipantControlUnitConfig CreateTrafficParticipantControlConfig()
    {
        controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
        tpm_control_unit_config.gtgen_map = gtgen_map_.get();
        tpm_control_unit_config.name = tpm_library_name;
        tpm_control_unit_config.traffic_command_builder = traffic_command_builder_.get();
        tpm_control_unit_config.proto_ground_truth_builder_config.entity_repository = &entity_repository_;
        tpm_control_unit_config.output_settings = &output_settings_;

        return tpm_control_unit_config;
    }

    mantle_api::IEntity* CreateTpmEntity(mantle_api::UniqueId id)
    {
        mantle_api::VehicleProperties vehicle_properties{};
        vehicle_properties.bounding_box.dimension = {2_m, 2_m, 1.5_m};

        return &entity_repository_.Create(id, std::string{tpm_library_name}, vehicle_properties);
    }

    static constexpr std::string_view tpm_library_name{"traffic_participant_model_lib"};

    service::utility::UniqueIdProvider unique_id_provider_{};
    api::EntityRepository entity_repository_{&unique_id_provider_};
    std::map<std::string, std::string> tpm_params_{{"velocity", "22"}};
    std::unique_ptr<map::GtGenMap> gtgen_map_{nullptr};
    std::unique_ptr<traffic_command::TrafficCommandBuilder> traffic_command_builder_{nullptr};
    datastore::OutputGenerator::Settings output_settings_{fs::path{}};
};

}  // namespace gtgen::core::environment::control_unit::test

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_BASE_FIXTURE_H
