/*******************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace gtgen::core::environment::controller
{

class CompositeControllerValidator
{
  public:
    static void DeleteConflictingControlUnits(const IControlUnit* control_unit,
                                              std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                              mantle_api::UniqueId unique_id);
    static void InsertControlUnitInOrder(std::unique_ptr<IControlUnit> control_unit,
                                         std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                         mantle_api::UniqueId unique_id);
    static bool NeedsPathControlUnit(std::vector<std::unique_ptr<IControlUnit>>& control_units);
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H
