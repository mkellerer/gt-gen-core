/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"

namespace gtgen::core::environment::datastore
{

CyclicResult::CyclicResult(CyclicRowRefs elements) : elements{std::move(elements)} {}

CyclicResult::~CyclicResult() = default;

std::size_t CyclicResult::size() const
{
    return elements.size();
}

const CyclicRow& CyclicResult::at(const std::size_t index) const
{
    return elements.at(index);
}

CyclicRowRefs::const_iterator CyclicResult::begin() const
{
    return elements.cbegin();
}

CyclicRowRefs::const_iterator CyclicResult::end() const
{
    return elements.cend();
}

AcyclicResult::AcyclicResult(AcyclicRowRefs elements) : elements{std::move(elements)} {}

AcyclicResult::~AcyclicResult() = default;

std::size_t AcyclicResult::size() const
{
    return elements.size();
}

const AcyclicRow& AcyclicResult::at(const std::size_t index) const
{
    return elements.at(index);
}

AcyclicRowRefs::const_iterator AcyclicResult::begin() const
{
    return elements.cbegin();
}

AcyclicRowRefs::const_iterator AcyclicResult::end() const
{
    return elements.cend();
}

BasicDataBufferImplementation::BasicDataBufferImplementation() = default;

BasicDataBufferImplementation::~BasicDataBufferImplementation() = default;

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(const mantle_api::UniqueId entity_id,
                                                                                const Key& key) const
{
    CyclicRowRefs row_refs;

    for (const CyclicRow& store_value : cyclic_store_)
    {
        if (entity_id == store_value.entity_id && (key == store_value.key || key == kWildcard))
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<CyclicResult>(row_refs);
}

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(const Key& key) const
{
    CyclicRowRefs row_refs;

    for (const CyclicRow& store_value : cyclic_store_)
    {
        if (key == store_value.key || key == kWildcard)
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<CyclicResult>(row_refs);
}

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(
    const std::optional<mantle_api::UniqueId> entity_id,
    const Key& key) const
{
    return entity_id.has_value() ? GetCyclic(entity_id.value(), key) : GetCyclic(key);
}

std::unique_ptr<AcyclicResultInterface> BasicDataBufferImplementation::GetAcyclic(
    const std::optional<mantle_api::UniqueId> entity_id,
    const Key& key) const
{
    return entity_id.has_value() ? GetAcyclic(entity_id.value(), key) : GetAcyclic(key);
}

std::unique_ptr<AcyclicResultInterface> BasicDataBufferImplementation::GetAcyclic(const Key& key) const
{
    AcyclicRowRefs row_refs;

    for (const AcyclicRow& store_value : acyclic_store_)
    {
        if (key == store_value.key || key == kWildcard)
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<AcyclicResult>(row_refs);
}

std::unique_ptr<AcyclicResultInterface> BasicDataBufferImplementation::GetAcyclic(const mantle_api::UniqueId entity_id,
                                                                                  const Key& key) const
{
    AcyclicRowRefs row_refs;

    for (const AcyclicRow& store_value : acyclic_store_)
    {
        if (entity_id == store_value.entity_id && (key == store_value.key || key == kWildcard))
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<AcyclicResult>(row_refs);
}

void BasicDataBufferImplementation::PutCyclic(const mantle_api::UniqueId agentId, const Key& key, const Value& value)
{
    cyclic_store_.emplace_back(agentId, key, value);
}

void BasicDataBufferImplementation::PutAcyclic(const mantle_api::UniqueId entity_id,
                                               const Key& key,
                                               const Acyclic& acyclic)
{
    acyclic_store_.emplace_back(entity_id, key, acyclic);
}

void BasicDataBufferImplementation::Clear()
{
    cyclic_store_.clear();
    acyclic_store_.clear();
}

}  // namespace gtgen::core::environment::datastore
