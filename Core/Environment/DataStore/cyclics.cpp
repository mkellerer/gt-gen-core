/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/cyclics.h"

#include <utility>

namespace gtgen::core::environment::datastore
{

Cyclics::Cyclics() = default;

Cyclics::~Cyclics() = default;

std::string Cyclics::GetHeader() const
{
    std::string header;
    for (auto it = samples_.begin(); it != samples_.end(); ++it)
    {
        if (it != samples_.begin())
        {
            header += ", ";
        }

        header += it->first;
    }
    return header;
}

std::string Cyclics::GetSample(std::uint32_t time_step_number, const std::string& key) const
{
    auto row = samples_.find(key);
    if (row == samples_.cend() || time_step_number >= row->second.size())
    {
        return "";
    }
    return row->second.at(time_step_number);
}

std::string Cyclics::GetSamplesLine(std::uint32_t time_step_number) const
{
    std::string sample_line;
    for (auto it = samples_.begin(); it != samples_.end(); ++it)
    {
        const std::vector<std::string>& values = it->second;

        if (it != samples_.begin())
        {
            sample_line += ", ";
        }

        // not all channels are sampled until end of simulation time
        if (time_step_number < values.size())
        {
            sample_line += values.at(time_step_number);
        }
    }

    return sample_line;
}

void Cyclics::Clear()
{
    time_steps_.clear();
    samples_.clear();
}

void Cyclics::Insert(std::uint32_t time, const std::string& key, const std::string& value)
{
    time_steps_.insert(time_steps_.end(), time);

    auto it = samples_.find(key);

    if (it == samples_.end())
    {
        auto [new_it, new_element] = samples_.emplace(key, time_steps_.size() - 1);
        new_it->second.push_back(value);
    }
    else
    {
        // fill up skipped time steps (e.g. another agent has been instantiated in between -> inserted new time step in
        // scheduling)
        it->second.resize(time_steps_.size() - 1);
        it->second.push_back(value);
    }
}

}  // namespace gtgen::core::environment::datastore
