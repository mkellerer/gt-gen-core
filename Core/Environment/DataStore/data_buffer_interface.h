/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H

#include <MantleAPI/Common/i_identifiable.h>

#include <cstddef>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace gtgen::core::environment::datastore
{

using Key = std::string;  ///< Type of a key in the DataBuffer
using Value = std::variant<bool,
                           std::vector<bool>,
                           char,
                           std::vector<char>,
                           int,
                           std::vector<int>,
                           std::size_t,
                           std::vector<std::size_t>,
                           float,
                           std::vector<float>,
                           double,
                           std::vector<double>,
                           std::string,
                           std::vector<std::string>>;  ///< Type of a value in the DataBuffer
using Parameter = std::map<Key, Value>;  ///< Type for parameters used in the DataBuffer (i.e. event parameters)

inline const Key kWildcard = "*";  //!< Wildcard to match any key string. Length of 1 is mandatory.

/*!
 * \brief Represents an acyclic occurence, like an event
 */
class Acyclic
{
  public:
    Acyclic() = default;

    /**
     * @brief Construct a new Acyclic object
     *
     * @param name                Name (or identifier) of this occurence
     * @param triggering_entities List of entities causing this occurence
     * @param affected_entities   List of entities affected by this occurence
     * @param parameter           Generic parameter set associated with this occurence
     */
    Acyclic(std::string name,
            std::vector<mantle_api::UniqueId> triggering_entities,
            std::vector<mantle_api::UniqueId> affected_entities,
            Parameter parameter)
        : name{std::move(name)},
          triggering_entities{std::move(triggering_entities)},
          affected_entities{std::move(affected_entities)},
          parameter{std::move(parameter)}
    {
    }

    /**
     * @brief Construct a new Acyclic object
     *
     * @param name      Name (or identifier) of this occurence
     * @param entity    New entity causing this occurance
     * @param parameter Generic parameter set associated with this occurence
     */
    Acyclic(std::string name, mantle_api::UniqueId entity, Parameter parameter)
        : name{std::move(name)}, parameter{std::move(parameter)}
    {
        triggering_entities.push_back(entity);
    }

    /**
     * @brief Function to compare the equality of two acyclic objects
     *
     * @param other Another object
     * @return
     */
    bool operator==(const Acyclic& other) const
    {
        return name == other.name && triggering_entities == other.triggering_entities &&
               affected_entities == other.affected_entities;
    }

    std::string name;                                       //!< Name (or identifier) of this occurence
    std::vector<mantle_api::UniqueId> triggering_entities;  //!< List of entities causing this occurence
    std::vector<mantle_api::UniqueId> affected_entities;    //!< List of entities affected by this occurence
    Parameter parameter;                                    //!< Generic parameter set associated with this occurence
};

/*!
 * \brief Representation of an entry in the acyclics
 */
struct AcyclicRow
{
    /**
     * @brief Construct a new Acyclic Row object
     *
     * @param id    Id of the entity (agent or object)
     * @param k     Key (topic) associated with the data
     * @param data  Acyclic data container
     */
    AcyclicRow(mantle_api::UniqueId id, Key k, Acyclic data) : entity_id{id}, key{k}, data{data} {}

    /**
     * @brief Function to compare two entries in the acyclics
     *
     * @param other Another acyclic row
     * @return
     */
    bool operator==(const AcyclicRow& other) const
    {
        return entity_id == other.entity_id && key == other.key && data == other.data;
    }

    mantle_api::UniqueId entity_id;  //!< Id of the entity (agent or object)
    Key key;                         //!< Key (topic) associated with the data
    Acyclic data;                    //!< Acyclic data container
};

/// @brief Representation of an entry in the cyclics.
///
struct CyclicRow
{
    /// @brief Construct a new Cyclic Row object.
    ///
    /// @param[in] id     Id of the entity (agent or object)
    /// @param[in] k      Key (topic) associated with the data
    /// @param[in] v      Data value
    CyclicRow(mantle_api::UniqueId id, Key k, Value v);

    bool operator==(const CyclicRow& other) const
    {
        return entity_id == other.entity_id && key == other.key && value == other.value;
    }

    mantle_api::UniqueId entity_id;  //!< Id of the entity (agent or object)
    Key key;                         //!< Key (topic) associated with the data
    Value value;                     //!< Data value
};

using Keys = std::vector<Key>;                                                 //!< List of keys
using Values = std::vector<Value>;                                             //!< List of values
using CyclicRowRefs = std::vector<std::reference_wrapper<const CyclicRow>>;    //!< List of references to rows
using AcyclicRowRefs = std::vector<std::reference_wrapper<const AcyclicRow>>;  //!< List of references to acyclic rows

/// @brief A set of cyclic data elements representing a DataInterface query result.
/// Basic forward iterator properties are provided for convenient result iteration.
///
class CyclicResultInterface
{
  public:
    CyclicResultInterface();
    CyclicResultInterface(const CyclicResultInterface&) = delete;
    CyclicResultInterface(CyclicResultInterface&&) = delete;
    CyclicResultInterface& operator=(const CyclicResultInterface&) & = delete;
    CyclicResultInterface& operator=(CyclicResultInterface&&) & = delete;
    virtual ~CyclicResultInterface();

    /// @return Returns size of cyclic result interface
    virtual std::size_t size() const = 0;

    /// @param[in] index position of cyclic result interface
    /// @return Returns the reference to cyclic row at given point
    virtual const CyclicRow& at(const std::size_t index) const = 0;

    /// @return Returns constant iterator of cyclic row
    virtual CyclicRowRefs::const_iterator begin() const = 0;

    /// @return Returns constant iterator of cyclic row
    virtual CyclicRowRefs::const_iterator end() const = 0;
};
class AcyclicResultInterface
{
  public:
    virtual ~AcyclicResultInterface() = default;

    /// @return Returns size of Acyclic result interface
    virtual std::size_t size() const = 0;

    /// @param index position of acyclic result interface
    /// @return Returns the reference to acyclic row at given point
    virtual const AcyclicRow& at(const std::size_t index) const = 0;

    /// @return Returns constant iterator of acyclic row
    virtual AcyclicRowRefs::const_iterator begin() const = 0;

    /// @return Returns constant iterator of acyclic row
    virtual AcyclicRowRefs::const_iterator end() const = 0;
};

/// @brief The DataBufferReadInterface provides read-only access to the data.
///
class DataBufferReadInterface
{
  public:
    DataBufferReadInterface();
    DataBufferReadInterface(const DataBufferReadInterface&) = delete;
    DataBufferReadInterface(DataBufferReadInterface&&) = delete;
    DataBufferReadInterface& operator=(const DataBufferReadInterface&) & = delete;
    DataBufferReadInterface& operator=(DataBufferReadInterface&&) & = delete;
    virtual ~DataBufferReadInterface();

    /// @brief Retrieves stored cyclic values.
    ///
    /// @param[in]   entity_id   Entity's id
    /// @param[in]   key         Unique topic identification
    /// @return stored cyclic values
    virtual std::unique_ptr<CyclicResultInterface> GetCyclic(const std::optional<mantle_api::UniqueId> entity_id,
                                                             const Key& key) const = 0;
    /*!
     * @brief Retrieves stored acyclic values
     *
     * @param[in]   entity_id   Entity's id
     * @param[in]   key         Unique topic identification
     *
     * @return stored acyclic values
     */
    virtual std::unique_ptr<AcyclicResultInterface> GetAcyclic(const std::optional<mantle_api::UniqueId> entity_id,
                                                               const Key& key) const = 0;
};

/// @brief The DataBufferWriteInterface provides write-only access to the data.
///
class DataBufferWriteInterface
{
  public:
    DataBufferWriteInterface();
    DataBufferWriteInterface(const DataBufferWriteInterface&) = delete;
    DataBufferWriteInterface(DataBufferWriteInterface&&) = delete;
    DataBufferWriteInterface& operator=(const DataBufferWriteInterface&) & = delete;
    DataBufferWriteInterface& operator=(DataBufferWriteInterface&&) & = delete;
    virtual ~DataBufferWriteInterface();

    /// @brief Writes cyclic information
    ///
    /// @param[in]   entity_id   Id of the associated agent or object
    /// @param[in]   key         Unique topic identification
    /// @param[in]   value       Value to be written
    virtual void PutCyclic(const mantle_api::UniqueId entity_id, const Key& key, const Value& value) = 0;

    /*!
     * @brief Writes acyclic information
     *
     * @param[in]   entity_id   Id of the associated agent
     * @param[in]   key         Unique topic identification
     * @param[in]   acyclic     The acyclic element to be written
     */
    virtual void PutAcyclic(const mantle_api::UniqueId entity_id, const Key& key, const Acyclic& acyclic) = 0;
};

/// @brief The DataInterface provides read/write access to the data.
/// This interface combines DataReadInterface and DataWriteInterface and adds some additional
/// methods required for instantiation by the framework.
///
class DataBufferInterface : public DataBufferReadInterface, public DataBufferWriteInterface
{
  public:
    DataBufferInterface();
    DataBufferInterface(const DataBufferInterface&) = delete;
    DataBufferInterface(DataBufferInterface&&) = delete;
    DataBufferInterface& operator=(const DataBufferInterface&) & = delete;
    DataBufferInterface& operator=(DataBufferInterface&&) & = delete;
    ~DataBufferInterface() override;

    /// @brief Clears the data contents.
    virtual void Clear() = 0;
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H
