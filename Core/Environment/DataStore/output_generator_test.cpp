/********************************************************************************
 * Copyright (c) 2024-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Core/Environment/DataStore/output_generator.h"

#include "Core/Environment/DataStore/mock_data_buffer_interface.h"
#include "Core/Environment/DataStore/test_utils.h"
#include "Core/Environment/DataStore/utils.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <fmt/format.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::datastore
{

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::StrictMock;

std::string GetCsvFilename(const std::int32_t run_number, const std::int32_t amount_digits)
{
    std::stringstream ss{};
    ss << "Cyclics_Run_" << std::setfill('0') << std::setw(amount_digits) << run_number << ".csv";
    return ss.str();
}

std::string GetEventsFileName(const std::int32_t run_number, const std::int32_t digits)
{
    std::stringstream ss{};
    ss << "Events_Run_" << std::setfill('0') << std::setw(digits) << run_number << ".json";
    return ss.str();
}

class OutputGeneratorTest : public ::testing::Test
{
  protected:
    fs::path output_path = fs::temp_directory_path() / "OutputGeneratorTestFolder";

    std::string GetCoreInformationPath() const { return output_path / "CoreInformation.json"; }

    void TearDown() override { fs::remove_all(output_path); }
};

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenEmptyAcyclics_ThenAcyclicsOutputIsNotGenerated)
{
    const mantle_api::Time time{100};

    OutputGenerator output_generator{{output_path}};

    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    const std::string expected_path = (output_path / GetEventsFileName(0, 1)).string();

    ASSERT_FALSE(fs::exists(expected_path)) << "File exists even for empty acyclics: " << expected_path;
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenEmptyCyclics_ThenCyclicsOutputIsNotGenerated)
{
    const mantle_api::Time time{100};

    OutputGenerator output_generator{{output_path}};

    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    const std::string expected_path = (output_path / GetCsvFilename(0, 1)).string();

    ASSERT_FALSE(fs::exists(expected_path)) << "File exists even for empty cyclics: " << expected_path;
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenCyclics_ThenStoresCyclicsCorrectly)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const mantle_api::Time time{100};
    const std::int32_t run_number = 0;
    const fs::path output_file_path = output_path / GetCsvFilename(run_number, 1);
    const std::vector<std::string> expected_csv{
        fmt::format("Timestep, {}:{}", entity_id, key),
        fmt::format("{}, {}", time.to<int>(), std::to_string(std::get<double>(value)))};

    OutputGenerator output_generator{{output_path}};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    ASSERT_NO_THROW(output_generator.PutCyclic(entity_id, key, value));
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(output_file_path)) << "File does not exist: " << (output_file_path).string();
    EXPECT_EQ(expected_csv, test_utils::readCSV(output_file_path));
    fs::remove(output_file_path);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenAcyclics_ThenStoresAcyclicsCorrectly)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const mantle_api::Time time{100};
    const fs::path output_file_path = output_path / GetEventsFileName(0, 1);
    Acyclic acyclic_data;
    acyclic_data.name = "AcyclicData";
    acyclic_data.triggering_entities = {entity_id};
    acyclic_data.parameter.insert({"CollisionWithAgent", true});

    OutputGenerator output_generator{{output_path}};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    ASSERT_NO_THROW(output_generator.PutAcyclic(entity_id, "Collision", acyclic_data););
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(output_file_path)) << "File does not exist: " << (output_file_path).string();

    fs::remove(output_file_path);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenMoreRuns_ThenOutputMoreAcyclicFiles)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const mantle_api::Time time{100};
    const std::int32_t run_0 = 0;
    const std::int32_t run_1 = 1;
    const std::int32_t total_runs = 11;

    const fs::path output_file_path_0 = output_path / GetEventsFileName(run_0, 2);
    const fs::path output_file_path_1 = output_path / GetEventsFileName(run_1, 2);

    Acyclic acyclic_data;
    acyclic_data.name = "AcyclicData";
    acyclic_data.triggering_entities = {entity_id};
    acyclic_data.parameter.insert({"CollisionWithAgent", true});

    OutputGenerator output_generator_0{{output_path, run_0, total_runs}};
    OutputGenerator output_generator_1{{output_path, run_1, total_runs}};
    // When

    ASSERT_NO_THROW(output_generator_0.Init());
    ASSERT_NO_THROW(output_generator_0.PutAcyclic(entity_id, "Collision", acyclic_data););
    ASSERT_NO_THROW(output_generator_0.Step(time));
    ASSERT_NO_THROW(output_generator_0.FinishRun());

    ASSERT_NO_THROW(output_generator_1.Init());
    ASSERT_NO_THROW(output_generator_1.PutAcyclic(entity_id, "Collision", acyclic_data););
    ASSERT_NO_THROW(output_generator_1.Step(time));
    ASSERT_NO_THROW(output_generator_1.FinishRun());
    // Expect

    ASSERT_TRUE(fs::exists(output_file_path_1)) << "File does not exist: " << (output_file_path_1).string();
    ASSERT_TRUE(fs::exists(output_file_path_0)) << "File does not exist: " << (output_file_path_0).string();

    fs::remove(output_file_path_1);
    fs::remove(output_file_path_0);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenMoreRuns_ThenOutputMoreFiles)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const CyclicRow row{entity_id, key, value};
    const CyclicRowRefs row_refs{row};
    const mantle_api::Time time{100};
    const std::int32_t run_0 = 0;
    const std::int32_t run_1 = 1;
    const std::int32_t total_runs = 2;
    const fs::path expected_output_file_path_0 = output_path / GetCsvFilename(run_0, 1);
    const fs::path expected_output_file_path_1 = output_path / GetCsvFilename(run_1, 1);
    const std::vector<std::string> expected_csv_0{
        fmt::format("Timestep, {}:{}", entity_id, key),
        fmt::format("{}, {}", time.to<int>(), std::to_string(std::get<double>(value)))};
    const std::vector<std::string> expected_csv_1{
        fmt::format("Timestep, {}:{}", entity_id, key),
        fmt::format("{}, {}", time.to<int>(), std::to_string(std::get<double>(value)))};

    OutputGenerator output_generator_0{{output_path, run_0, total_runs}};
    OutputGenerator output_generator_1{{output_path, run_1, total_runs}};

    // When

    ASSERT_NO_THROW(output_generator_0.Init());
    ASSERT_NO_THROW(output_generator_0.PutCyclic(entity_id, key, value));
    ASSERT_NO_THROW(output_generator_0.Step(time));
    ASSERT_NO_THROW(output_generator_0.FinishRun());

    ASSERT_NO_THROW(output_generator_1.Init());
    ASSERT_NO_THROW(output_generator_1.PutCyclic(entity_id, key, value));
    ASSERT_NO_THROW(output_generator_1.Step(time));
    ASSERT_NO_THROW(output_generator_1.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(expected_output_file_path_0))
        << "File does not exist: " << (expected_output_file_path_0).string();
    EXPECT_EQ(expected_csv_0, test_utils::readCSV(expected_output_file_path_0));
    fs::remove(expected_output_file_path_0);

    ASSERT_TRUE(fs::exists(expected_output_file_path_1))
        << "File does not exist: " << (expected_output_file_path_1).string();
    EXPECT_EQ(expected_csv_1, test_utils::readCSV(expected_output_file_path_1));
    fs::remove(expected_output_file_path_1);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenNonZeroNumber_ThenOutputFileWithSameNumber)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const CyclicRow row{entity_id, key, value};
    const CyclicRowRefs row_refs{row};
    const mantle_api::Time time{100};
    std::int32_t run_number = 999;
    const fs::path output_file_path = output_path / GetCsvFilename(run_number, 3);
    const std::vector<std::string> expected_csv{
        fmt::format("Timestep, {}:{}", entity_id, key),
        fmt::format("{}, {}", time.to<int>(), std::to_string(std::get<double>(value)))};

    OutputGenerator output_generator{{output_path, run_number, run_number + 1}};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    ASSERT_NO_THROW(output_generator.PutCyclic(entity_id, key, value));
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(output_file_path)) << "File does not exist: " << (output_file_path).string();
    EXPECT_EQ(expected_csv, test_utils::readCSV(output_file_path));
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenBigNumber_ThenOutputFileStillCreated)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const CyclicRow row{entity_id, key, value};
    const CyclicRowRefs row_refs{row};
    const mantle_api::Time time{100};
    const std::int32_t run_number = 999000;
    const fs::path output_file_path = output_path / GetCsvFilename(run_number, 6);

    const std::vector<std::string> expected_csv{
        fmt::format("Timestep, {}:{}", entity_id, key),
        fmt::format("{}, {}", time.to<int>(), std::to_string(std::get<double>(value)))};

    OutputGenerator output_generator{{output_path, run_number, run_number + 1}};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    ASSERT_NO_THROW(output_generator.PutCyclic(entity_id, key, value));
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(output_file_path)) << "File does not exist: " << (output_file_path).string();
    EXPECT_EQ(expected_csv, test_utils::readCSV(output_file_path));
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenInvalidArguments_ThenThrowException)
{
    const auto create_generator = [&output_path = output_path](std::int32_t run_number, std::int32_t total_runs = 1) {
        return OutputGenerator{{output_path, run_number, total_runs}};
    };

    EXPECT_THROW(create_generator(-1), EnvironmentException);
    EXPECT_THROW(create_generator(-1, -1), EnvironmentException);
    EXPECT_THROW(create_generator(0, 0), EnvironmentException);
    EXPECT_THROW(create_generator(1, 1), EnvironmentException);
    EXPECT_THROW(create_generator(1, 0), EnvironmentException);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenEmptyInfo_ThenCoreInformationFileNotGenerated)
{
    OutputGenerator output_generator{{output_path}};

    output_generator.Init();
    output_generator.FinishRun();

    ASSERT_FALSE(fs::exists(GetCoreInformationPath()))
        << "File created even for empty json: " << GetCoreInformationPath();
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenEmptySource_ThenEmptyCoreInformation)
{
    OutputGenerator output_generator{{output_path}};

    output_generator.Init();

    ASSERT_TRUE(output_generator.GetCoreInformation().empty());
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenNonEmptyInfo_ThenCoreInformationFileGenerated)
{
    OutputGenerator output_generator{{output_path}};

    output_generator.Init();
    output_generator.SetCoreInformation({{"key_0", "value_0"}});
    output_generator.FinishRun();

    ASSERT_TRUE(fs::exists(GetCoreInformationPath())) << "File does not exist: " << GetCoreInformationPath();
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenNonEmptyInfo_ThenCoreInformationFileGeneratedCorrectly)
{
    OutputGenerator output_generator{{output_path}};

    const nlohmann::json orig_json = {{"key_1", "value_1"}};

    output_generator.Init();
    output_generator.SetCoreInformation(orig_json);
    output_generator.FinishRun();

    ASSERT_TRUE(fs::exists(GetCoreInformationPath()));

    nlohmann::json parsed_json{};
    ASSERT_NO_THROW(parsed_json = utils::ParseJson(GetCoreInformationPath(), true));

    EXPECT_EQ(orig_json, parsed_json);
}

TEST_F(OutputGeneratorTest, GivenOutputGenerator_WhenNonEmptyOutputFolder_ThenCoreInformationFileParsedCorrectly)
{
    const nlohmann::ordered_json source_json = {{"key_2", "value_2"}};
    fs::create_directories(output_path);
    utils::WriteJson(source_json, GetCoreInformationPath());

    OutputGenerator output_generator{{output_path}};
    output_generator.Init();

    EXPECT_EQ(source_json, output_generator.GetCoreInformation());
}

}  // namespace gtgen::core::environment::datastore
