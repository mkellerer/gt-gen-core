/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_TESTUTILS_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_TESTUTILS_H

#include "Core/Service/FileSystem/filesystem.h"

#include <fstream>
#include <string>
#include <vector>

namespace gtgen::core::environment::datastore::test_utils
{

/// @brief Attempt to read the contents of the .csv file
/// specified by @p output_file_path.
/// @param[in] output_file_path The path to the .csv file
/// @return The lines of the .csv file.
std::vector<std::string> readCSV(const fs::path& output_file_path);

}  // namespace gtgen::core::environment::datastore::test_utils

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_TESTUTILS_H
