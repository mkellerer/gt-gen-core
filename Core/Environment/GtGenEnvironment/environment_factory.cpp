/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/environment_factory.h"

#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"

namespace gtgen::core::environment::api
{

std::unique_ptr<GtGenEnvironment> EnvironmentFactory::Create(const service::user_settings::UserSettings& user_settings,
                                                             const mantle_api::Time step_size,
                                                             const std::uint32_t seed,
                                                             const std::int32_t run_number,
                                                             const std::int32_t total_runs)
{
    return std::make_unique<GtGenEnvironment>(user_settings, step_size, seed, run_number, total_runs);
}
}  // namespace gtgen::core::environment::api
