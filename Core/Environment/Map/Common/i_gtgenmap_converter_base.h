/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IGTGENMAPCONVERTERBASE_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IGTGENMAPCONVERTERBASE_H

#include "Core/Environment/Map/Common/i_any_to_gtgenmap_converter.h"
#include "Core/Environment/Map/Common/map_converter_data.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map_finalizer.h"
#include "Core/Service/Logging/scoped_logging_context.h"

namespace gtgen::core::environment::map
{
class GtGenMap;

class IGtGenMapConverterBase : public IAnyToGtGenMapConverter
{
  public:
    IGtGenMapConverterBase(service::utility::UniqueIdProvider& id_provider,
                           const MapConverterData& data,
                           environment::map::GtGenMap& gtgen_map)
        : unique_id_provider_{id_provider}, data_{data}, gtgen_map_{gtgen_map}
    {
    }

    void Convert() override
    {
        service::logging::ScopedLoggingContext scoped_logging_context_guard("map");

        PreConvert();
        ConvertInternal();
        PostConvert();
    }

  protected:
    virtual void PreConvert() = 0;

    virtual void PostConvert()
    {
        GtGenMapFinalizer finalizer(gtgen_map_);
        finalizer.Finalize();
    }

    virtual void ConvertInternal() = 0;

    service::utility::UniqueIdProvider& unique_id_provider_;
    MapConverterData data_;
    environment::map::GtGenMap& gtgen_map_;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IGTGENMAPCONVERTERBASE_H
