/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H

#include "Core/Service/MantleApiExtension/json_map_details.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Map/map_details.h>

#include <string>
#include <vector>

namespace gtgen::core::environment::map
{

struct MapConverterData
{
    std::string absolute_map_path{};
    std::vector<mantle_api::LatLonPosition> waypoints{};
    std::vector<mantle_ext::FrictionPatch> friction_patches{};

    double lane_marking_distance_in_m{};
    double lane_marking_downsampling_epsilon{};
    bool lane_marking_downsampling{true};
    bool include_obstacles{false};

    mantle_api::MapRegionType map_region_type;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H
