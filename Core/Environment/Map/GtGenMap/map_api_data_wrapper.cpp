/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/map_api_data_wrapper.h"

namespace gtgen::core::environment::map
{

void MapApiDataWrapper::RefreshReferencesForLogicalLane(map_api::LogicalLane& logical_lane)
{
    auto internal_logical_lane = Find<map_api::LogicalLane>(logical_lane.id);

    if (!internal_logical_lane)
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(exception::MapElementNotFound(
            "No corresponding logical lane added internally, could not refresh LogicalLane with ID {}",
            logical_lane.id));
    }

    if (logical_lane.reference_line)
    {
        internal_logical_lane->reference_line = Find<map_api::ReferenceLine>(logical_lane.reference_line->id);
    }

    auto refreshLaneRelations = [&](const auto& source, auto& target) {
        target.clear();
        target.reserve(source.size());
        for (const auto& element : source)
        {
            target.emplace_back(map_api::LogicalLaneRelation{Get<map_api::LogicalLane>(element.other_lane.id),
                                                             element.start_s,
                                                             element.end_s,
                                                             element.start_s_other,
                                                             element.end_s_other});
        }
    };

    internal_logical_lane->physical_lane_references.clear();
    internal_logical_lane->physical_lane_references.reserve(logical_lane.physical_lane_references.size());
    for (const auto& element : logical_lane.physical_lane_references)
    {
        internal_logical_lane->physical_lane_references.emplace_back(map_api::PhysicalLaneReference{
            Get<map_api::Lane>(element.physical_lane.id), element.start_s, element.end_s});
    }

    refreshLaneRelations(logical_lane.right_adjacent_lanes, internal_logical_lane->right_adjacent_lanes);
    refreshLaneRelations(logical_lane.left_adjacent_lanes, internal_logical_lane->left_adjacent_lanes);
    refreshLaneRelations(logical_lane.overlapping_lanes, internal_logical_lane->overlapping_lanes);

    RefreshReferences(internal_logical_lane->right_boundaries);
    RefreshReferences(internal_logical_lane->left_boundaries);
    RefreshReferences(internal_logical_lane->predecessor_lanes);
    RefreshReferences(internal_logical_lane->successor_lanes);
}

void MapApiDataWrapper::ClearMap()
{
    reference_lines_.clear();
    lanes_.clear();
    lane_boundaries_.clear();
    logical_lanes_.clear();
    logical_lane_boundaries_.clear();

    id_reference_line_index_map_.clear();
    id_lane_index_map_.clear();
    id_lane_boundary_index_map_.clear();
    id_logical_lane_index_map_.clear();
    id_logical_lane_boundary_index_map_.clear();
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::ReferenceLine>() const
{
    return id_reference_line_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::Lane>() const
{
    return id_lane_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::LaneBoundary>() const
{
    return id_lane_boundary_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::LogicalLane>() const
{
    return id_logical_lane_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::LogicalLaneBoundary>() const
{
    return id_logical_lane_boundary_index_map_;
}

template <>
const std::vector<map_api::ReferenceLine>& MapApiDataWrapper::GetDataContainer() const
{
    return reference_lines_;
}

template <>
const std::vector<map_api::Lane>& MapApiDataWrapper::GetDataContainer() const
{
    return lanes_;
}

template <>
const std::vector<map_api::LaneBoundary>& MapApiDataWrapper::GetDataContainer() const
{
    return lane_boundaries_;
}

template <>
const std::vector<map_api::LogicalLane>& MapApiDataWrapper::GetDataContainer() const
{
    return logical_lanes_;
}

template <>
const std::vector<map_api::LogicalLaneBoundary>& MapApiDataWrapper::GetDataContainer() const
{
    return logical_lane_boundaries_;
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::ReferenceLine>() const
{
    return "ReferenceLine";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::Lane>() const
{
    return "Lane";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::LaneBoundary>() const
{
    return "LaneBoundary";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::LogicalLane>() const
{
    return "LogicalLane";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::LogicalLaneBoundary>() const
{
    return "LogicalLaneBoundary";
}

}  // namespace gtgen::core::environment::map
