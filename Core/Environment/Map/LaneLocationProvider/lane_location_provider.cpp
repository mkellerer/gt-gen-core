/*******************************************************************************
 * Copyright (c) 2019-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/LaneFollowing/find_longest_path.h"
#include "Core/Environment/LaneFollowing/line_follow_data_from_lanes.h"
#include "Core/Environment/LaneFollowing/line_follow_data_from_path.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/GtGenMap/lane.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_finder.h"
#include "Core/Environment/PathFinding/path.h"
#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/math_utils.h"
#include "Core/Service/Utility/position_utils.h"

#include <bits/exception.h>
#include <fmt/format.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iterator>
#include <limits>
#include <numeric>
#include <queue>

namespace gtgen::core::environment::map
{

using units::literals::operator""_m;

LaneLocationProvider::LaneLocationProvider(const GtGenMap& map)
    : map_{map}, lane_finder_{std::make_unique<LaneFinder>(map)}
{
}

LaneLocationProvider::~LaneLocationProvider() = default;

LaneLocation LaneLocationProvider::GetLaneLocation(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    GTGEN_PROFILE_SCOPE

    if (last_request_position_ == position)
    {
        return cached_lane_location_;
    }

    auto lane = FindBestLaneCandidate(position);
    if (lane == nullptr)
    {
        return {};
    }

    auto right_lane_boundaries = map_.GetRightLaneBoundaries(lane);
    auto left_lane_boundaries = map_.GetLeftLaneBoundaries(lane);
    cached_lane_location_ =
        LaneLocation{position, lane->center_line, right_lane_boundaries, left_lane_boundaries, FindLanes(position)};

    last_request_position_ = position;
    return cached_lane_location_;
}

const Lane* LaneLocationProvider::GetCachedSuccessorLane(const Lane* lane) const
{
    return lane_finder_->GetCachedSuccessorLane(lane);
}

const Lane* LaneLocationProvider::FindLane(const mantle_api::UniqueId lane_id) const
{
    return lane_finder_->FindLane(lane_id);
}

std::vector<const Lane*> LaneLocationProvider::FindLanes(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    return lane_finder_->FindLanes(position);
}

const Lane* LaneLocationProvider::FindBestLaneCandidate(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lanes = FindLanes(position);

    if (lanes.empty())
    {
        return nullptr;
    }

    if (lanes.size() == 1)
    {
        return lanes.front();
    }

    const auto preferred_lane_const_iterator = std::find_if(lanes.cbegin(), lanes.cend(), [this](const auto lane) {
        return !lane->successors.empty() && lane_finder_->AnyLaneExists(lane->successors);
    });

    if (preferred_lane_const_iterator == lanes.cend())
    {
        return lanes.front();
    }
    else
    {
        return *preferred_lane_const_iterator;
    }
}

mantle_api::Orientation3<units::angle::radian_t> LaneLocationProvider::GetLaneOrientation(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane_location = GetLaneLocation(position);
    if (!lane_location.IsValid())
    {
        throw EnvironmentException(
            "Unable to get lane orientation for position {},{}. This position is not located withing the road "
            "geometry. Check if "
            "entities are allowed to leave the road (AllowInvalidLaneLocations in User Settings), otherwise please "
            "contact GTGen Support for further assistance.",
            position.x,
            position.y);
    }

    return lane_location.GetOrientation();
}

mantle_api::Vec3<units::length::meter_t> LaneLocationProvider::GetUpwardsShiftedLanePosition(
    const mantle_api::Vec3<units::length::meter_t>& position,
    double upwards_shift,
    bool allow_invalid_positions) const
{
    auto lane_location = GetLaneLocation(position);
    if (!lane_location.IsValid())
    {
        if (allow_invalid_positions)
        {
            lane_location.lane_normal = {0_m, 0_m, 1_m};
        }
        else
        {
            throw EnvironmentException(
                "Unable to align position {},{} to the road, as it is not located withing the road geometry. Check if "
                "entities are allowed to leave the road (AllowInvalidLaneLocations in User Settings), otherwise please "
                "contact GTGen Support for further assistance.",
                position.x,
                position.y);
        }
    }

    auto vertical_shift = lane_location.lane_normal * upwards_shift;
    if (position.z > 0.0_m || position.z < 0.0_m)
    {
        return position + vertical_shift;
    }

    mantle_api::Vec3<units::length::meter_t> new_position = {
        position.x, position.y, lane_location.projected_centerline_point.z};
    return new_position + vertical_shift;
}

bool LaneLocationProvider::IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane_location = GetLaneLocation(position);
    return lane_location.IsValid();
}

LaneLocation LaneLocationProvider::GetLaneLocationById(mantle_api::UniqueId lane_id,
                                                       const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane = FindLane(lane_id);
    if (lane == nullptr)
    {
        throw EnvironmentException(
            "Unable to find lane with id {}, please contact GTGen Support for further assistance.", lane_id);
    }

    auto right_lane_boundaries = map_.GetRightLaneBoundaries(lane);
    auto left_lane_boundaries = map_.GetLeftLaneBoundaries(lane);
    return LaneLocation{position, lane->center_line, right_lane_boundaries, left_lane_boundaries, {lane}};
}

std::vector<mantle_api::UniqueId> LaneLocationProvider::GetLaneIdsAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    std::vector<mantle_api::UniqueId> lane_ids{};

    const auto lane_location = GetLaneLocation(position);

    std::transform(lane_location.lanes.begin(),
                   lane_location.lanes.end(),
                   std::back_inserter(lane_ids),
                   [](const map::Lane* lane) { return lane->id; });

    return lane_ids;
}

std::vector<mantle_api::UniqueId> LaneLocationProvider::GetSortedLaneIdsAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation) const
{
    std::vector<mantle_api::UniqueId> sorted_lane_ids{};

    const auto lanes = GetLaneLocation(position).lanes;

    std::vector<std::vector<const environment::map::Lane*>> lane_groups_sorted_by_distance =
        FilterLanesByAltitude(lanes, position);

    for (const auto& group : lane_groups_sorted_by_distance)
    {
        const auto sorted_group = SortLaneIdsByAngle(group, position, orientation);
        sorted_lane_ids.insert(sorted_lane_ids.end(), sorted_group.begin(), sorted_group.end());
    }
    return sorted_lane_ids;
}

std::vector<std::vector<const environment::map::Lane*>> LaneLocationProvider::FilterLanesByAltitude(
    const std::vector<const environment::map::Lane*>& input_lanes,
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    std::vector<std::vector<const environment::map::Lane*>> lane_groups{};
    lane_groups.push_back(std::vector<const environment::map::Lane*>{});
    std::vector<std::pair<const environment::map::Lane*, units::length::meter_t>> filtered_by_distance{};
    for (const auto* lane : input_lanes)
    {
        auto point_with_index = environment::map::ProjectQueryPointOnPolyline(position, lane->center_line);
        auto distance_to_pos = position.z - std::get<0>(point_with_index).z;
        // Remove lanes which are more than 0.5m above the position
        if (distance_to_pos > -0.5_m)
        {
            filtered_by_distance.push_back(std::make_pair(lane, distance_to_pos));
        }
    }

    // Sort vector by the alltitude distance
    std::sort(
        filtered_by_distance.begin(),
        filtered_by_distance.end(),
        [](std::pair<const environment::map::Lane*, units::length::meter_t> a,
           std::pair<const environment::map::Lane*, units::length::meter_t> b) -> bool { return a.second < b.second; });

    // Group vector by distance
    auto grouping_distance = 0.5_m;
    std::size_t group_index = 0;
    auto last_distance = 0_m;
    for (auto pair : filtered_by_distance)
    {
        if (lane_groups[group_index].empty())
        {
            last_distance = pair.second;
            lane_groups[group_index].push_back(pair.first);
            continue;
        }
        if (pair.second - last_distance > grouping_distance)
        {
            group_index += 1;
            last_distance = pair.second;
            lane_groups.insert(lane_groups.end(), std::vector<const environment::map::Lane*>{});
        }
        lane_groups[group_index].insert(lane_groups[group_index].end(), pair.first);
    }
    return lane_groups;
}

std::vector<mantle_api::UniqueId> LaneLocationProvider::SortLaneIdsByAngle(
    const std::vector<const environment::map::Lane*>& lanes,
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation) const
{
    std::vector<mantle_api::UniqueId> sorted_lane_ids{};
    units::angle::radian_t smallest_angle = std::numeric_limits<units::angle::radian_t>::max();
    for (const auto* lane : lanes)
    {
        const auto lane_location{GetLaneLocationById(lane->id, position)};
        const auto lane_orientation{lane_location.GetOrientation()};
        const auto lane_yaw{lane_orientation.yaw};
        const auto entity_yaw{orientation.yaw};

        const units::angle::radian_t abs_angle{service::utility::AbsAngle(entity_yaw, lane_yaw)};

        if (abs_angle < smallest_angle)
        {
            sorted_lane_ids.insert(sorted_lane_ids.begin(), lane->id);
            smallest_angle = abs_angle;
        }
        else
        {
            sorted_lane_ids.push_back(lane->id);
        }
    }
    return sorted_lane_ids;
}

std::optional<mantle_api::Pose> LaneLocationProvider::GetProjectedPoseAtLane(
    const mantle_api::Vec3<units::length::meter_t>& reference_position_on_lane,
    mantle_api::LaneId target_lane_id) const
{
    const auto source_lane_location = GetLaneLocation(reference_position_on_lane);
    if (!source_lane_location.IsValid())
    {
        Warn("Start position ({}) is not on a valid lane location.", reference_position_on_lane);
        return std::nullopt;
    }

    const auto target_lane = lane_finder_->FindLane(target_lane_id);
    if (target_lane == nullptr)
    {
        Warn("Target lane {} does not exist.", target_lane_id);
        return std::nullopt;
    }

    const auto source_left_vector = service::glmwrapper::GetWorldSpaceLeftVector(source_lane_location.GetOrientation());

    const auto projected_position_2d = service::utility::GetLineToLineSegmentsIntersection2D(
        reference_position_on_lane, reference_position_on_lane + source_left_vector, target_lane->center_line);
    if (!projected_position_2d.has_value())
    {
        Warn("Can not calculate the projected position from the reference point {} to the target lane {}.",
             reference_position_on_lane,
             target_lane_id);
        return std::nullopt;
    }
    const auto project_lane_location = GetLaneLocation(projected_position_2d.value());
    if (!project_lane_location.IsValid())
    {
        Warn("Project position {} is not on a valid lane location.", projected_position_2d.value());
        return std::nullopt;
    }
    const auto z_coordinate = project_lane_location.projected_centerline_point.z;
    const auto intersected_point_position = mantle_api::Vec3<units::length::meter_t>{
        projected_position_2d.value().x, projected_position_2d.value().y, z_coordinate};
    return mantle_api::Pose{intersected_point_position, project_lane_location.GetOrientation()};
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LaneLocationProvider::GetProjectedCenterLinePoint(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane_location = GetLaneLocation(position);
    if (!lane_location.IsValid())
    {
        Warn("Position ({}) is not on a valid lane location.", position);
        return std::nullopt;
    }

    return lane_location.projected_centerline_point;
}

std::optional<mantle_api::UniqueId> LaneLocationProvider::GetAdjacentRelativeLaneId(const Lane& lane,
                                                                                    int relative_lane_target) const
{
    if (relative_lane_target == 0)
    {
        return lane.id;
    }

    auto target_adjacent_id = std::numeric_limits<mantle_api::UniqueId>::max();
    std::queue<mantle_api::UniqueId> adjacent_lanes_list{};
    const auto is_target_lane_on_the_right = relative_lane_target < 0;
    auto next_lane = lane;
    for (auto count = std::abs(relative_lane_target); count > 0; --count)
    {
        if (is_target_lane_on_the_right)
        {
            if (!next_lane.right_adjacent_lanes.empty())
            {
                ///@note currently only the first adjacent lane is considered.
                adjacent_lanes_list.push(next_lane.right_adjacent_lanes.front());
            }
        }
        else
        {
            if (!next_lane.left_adjacent_lanes.empty())
            {
                ///@note currently only the first adjacent lane is considered.
                adjacent_lanes_list.push(next_lane.left_adjacent_lanes.front());
            }
        }
        if (adjacent_lanes_list.empty())
        {
            Warn("Cannot find the target lane by lateral shift {}.", relative_lane_target);
            return std::nullopt;
        }
        target_adjacent_id = adjacent_lanes_list.front();
        adjacent_lanes_list.pop();
        next_lane = map_.GetLane(target_adjacent_id);
    }

    return target_adjacent_id == std::numeric_limits<mantle_api::UniqueId>::max()
               ? std::nullopt  // should never happen
               : std::optional<mantle_api::UniqueId>(target_adjacent_id);
}

std::optional<mantle_api::LaneId> LaneLocationProvider::GetRelativeLaneId(
    const mantle_api::Pose& reference_pose_on_lane,
    int relative_lane_target) const
{
    const auto list_of_lanes = lane_finder_->FindLanes(reference_pose_on_lane.position);
    if (list_of_lanes.empty())
    {
        Warn("No lanes found at position ({})", reference_pose_on_lane.position);
        return std::nullopt;
    }
    auto target_lane_global_id = GetAdjacentRelativeLaneId(*list_of_lanes[0], relative_lane_target);
    if (!target_lane_global_id.has_value())
    {
        return std::nullopt;
    }
    auto& target_lane = map_.GetLane(target_lane_global_id.value());
    return target_lane.local_id;
}

std::optional<int> LaneLocationProvider::GetRelativeLaneTarget(const mantle_api::Vec3<units::length::meter_t>& position,
                                                               mantle_api::LaneId target_lane_id) const
{
    const auto list_of_lanes = lane_finder_->FindLanes(position);
    if (list_of_lanes.empty())
    {
        Warn("GetRelativeLaneTarget: No lanes found at position ({})", position);
        return std::nullopt;
    }
    const auto lane_at_position = *list_of_lanes[0];
    const auto current_lane_id = lane_at_position.local_id;

    if (map_.IsOpenDrive() && current_lane_id > 0)
    {
        return std::optional<int>(current_lane_id - target_lane_id);
    }

    return std::optional<int>(target_lane_id - current_lane_id);
}

std::optional<mantle_api::Pose> LaneLocationProvider::FindRelativeLanePoseAtDistanceFrom(
    const mantle_api::Pose& reference_pose_on_lane,
    int relative_lane_target,
    units::length::meter_t distance,
    units::length::meter_t lateral_offset) const
{
    const auto list_of_lanes = lane_finder_->FindLanes(reference_pose_on_lane.position);
    if (list_of_lanes.empty())
    {
        Warn("Reference position ({}) is not on any lane.", reference_pose_on_lane.position);
        return std::nullopt;
    }
    const auto initial_lane_id = list_of_lanes[0]->id;

    std::optional<mantle_api::Pose> translate_pose;
    if (distance == 0_m)
    {
        translate_pose = reference_pose_on_lane;
    }
    else
    {
        // Move longitudinally along lane centerline by distance
        translate_pose = FindLanePoseAtDistanceFrom(
            reference_pose_on_lane,
            units::math::abs(distance),
            distance > 0.0_m ? mantle_api::Direction::kForward : mantle_api::Direction::kBackwards);
    }

    if (!translate_pose.has_value())
    {
        Warn(
            "Cannot find a valid position after moving {}m longitudinally along the lane from the starting position "
            "({}).",
            reference_pose_on_lane.position,
            distance);
        return std::nullopt;
    }

    const auto list_of_lanes_translated = lane_finder_->FindLanes(translate_pose.value().position);
    if (list_of_lanes_translated.empty())
    {
        Warn("New position ({}) after longitudinal move is not on a lane.", translate_pose.value().position);
        return std::nullopt;
    }

    const auto current_lane_id = list_of_lanes_translated[0]->id;
    if (initial_lane_id != current_lane_id)
    {
        Warn("New position after longitudinal move is on another lane {}, whereas the starting lane is {}.",
             current_lane_id,
             initial_lane_id);
    }

    const auto relative_lane_id_target = GetAdjacentRelativeLaneId(*list_of_lanes_translated[0], relative_lane_target);
    if (!relative_lane_id_target.has_value())
    {
        Warn("Target lane id does not exist, where relative number is : {} and starting lane id is {}.",
             relative_lane_target,
             current_lane_id);
        return std::nullopt;
    }

    // Move laterally by relative_lane_target number of lanes.
    std::optional<mantle_api::Pose> projected_pose_on_relative_lane =
        GetProjectedPoseAtLane(translate_pose.value().position, relative_lane_id_target.value());
    if (!projected_pose_on_relative_lane.has_value())
    {
        Warn("Cannot calculate a projected position to the target lane {} at position ({}).",
             relative_lane_id_target.value(),
             translate_pose.value().position);
        return std::nullopt;
    }

    const auto lane_location = GetLaneLocation(projected_pose_on_relative_lane.value().position);
    // Move laterally from centerline by lateral_offset
    auto projected_position_on_relative_lane_shifted =
        service::utility::GetLateralShiftedPosition(projected_pose_on_relative_lane.value().position,
                                                    lane_location.direction,
                                                    lane_location.lane_normal,
                                                    lateral_offset);

    projected_pose_on_relative_lane.value().position = projected_position_on_relative_lane_shifted;
    return projected_pose_on_relative_lane;
}

lanefollowing::PointListTraverser LaneLocationProvider::GetLanePointTraverser(
    const LaneLocation& lane_location,
    units::length::meter_t distance,
    const mantle_api::Direction direction) const
{
    std::vector<const map::Lane*> longest_path;
    if (direction == mantle_api::Direction::kBackwards)
    {
        longest_path = lanefollowing::FindLongestBackwardsPath(map_, lane_location.lanes[0]);
    }
    else
    {
        longest_path = lanefollowing::FindLongestPath(map_, lane_location.lanes);
    }

    const auto point_distance_list =
        lanefollowing::CreateLineFollowDataFromLanes(longest_path, lane_location, direction);
    lanefollowing::PointListTraverser point_list_traverser(point_distance_list);
    point_list_traverser.Move(distance);
    return point_list_traverser;
}

mantle_api::Pose CalculatePose(const lanefollowing::PointListTraverser& traverser,
                               const LaneLocation& lane_location,
                               const mantle_api::Pose& reference_pose_on_lane)
{
    auto position = traverser.GetPosition();
    const auto forward = service::glmwrapper::Normalize(traverser.GetNextPoint() - position);
    const auto position_on_segment = forward * traverser.GetAdditionalDistance()();
    position += position_on_segment;

    // Needed to compute the lateral distance from the ref pose to the centerline because the find longest path
    // is following the center line
    const units::length::meter_t lateral_shift_distance{
        service::utility::GetDistance2D(lane_location.projected_centerline_point, reference_pose_on_lane.position)};
    const auto new_position = service::utility::GetLateralShiftedPosition(
        position, lane_location.direction, lane_location.lane_normal, lateral_shift_distance);

    const auto new_orientation = lane_location.GetOrientation();

    return mantle_api::Pose{new_position, new_orientation};
}

std::optional<mantle_api::Pose> LaneLocationProvider::CalculatePoseUsingTraverser(
    const LaneLocation& lane_location,
    units::length::meter_t distance,
    const mantle_api::Pose& reference_pose_on_lane,
    const mantle_api::Direction direction) const
{
    const auto point_list_traverser = GetLanePointTraverser(lane_location, distance, direction);

    if (!point_list_traverser.IsLastPointReached())
    {
        return CalculatePose(point_list_traverser, lane_location, reference_pose_on_lane);
    }
    else
    {
        Warn("Target position is beyond road network limits.");
        return std::nullopt;
    }
}

std::optional<mantle_api::Pose> LaneLocationProvider::FindLanePoseAtDistanceFrom(
    const mantle_api::Pose& reference_pose_on_lane,
    units::length::meter_t distance,
    mantle_api::Direction direction) const
{

    const auto lane_location = GetLaneLocation(reference_pose_on_lane.position);
    if (!lane_location.IsValid())
    {
        Warn("Reference position is not located on a valid lane.");
        return std::nullopt;
    }
    if (distance.value() < 0)
    {
        Warn("Distance should not be a negative number");
        return std::nullopt;
    }
    return CalculatePoseUsingTraverser(lane_location, distance, reference_pose_on_lane, direction);
}

std::optional<units::length::meter_t> LaneLocationProvider::GetLongitudinalLaneDistanceBetweenPositions(
    const mantle_api::Vec3<units::length::meter_t>& start_position,
    const mantle_api::Vec3<units::length::meter_t>& target_position) const
{
    const auto start_lane_location = GetLaneLocation(start_position);
    if (!start_lane_location.IsValid())
    {
        Warn("Start position ({}) is not located on a valid lane.", start_position);
        return std::nullopt;
    }

    auto longest_path = lanefollowing::FindLongestPath(map_, start_lane_location.lanes);
    path_finding::Path start_to_target_path{};

    std::optional<mantle_api::Pose> project_target_pose;
    for (const auto& lane : longest_path)
    {
        start_to_target_path.emplace_back(
            std::make_shared<path_finding::PathEntry>(lane, path_finding::EdgeType::kFollow));
        if (project_target_pose = GetProjectedPoseAtLane(target_position, static_cast<mantle_api::LaneId>(lane->id));
            project_target_pose)
        {
            break;
        }
    }
    if (!project_target_pose.has_value())
    {
        Warn("Target position ({}) is not located on a the lane-path from the start position.", target_position);
        return std::nullopt;
    }

    const auto target_lane_location = GetLaneLocation(project_target_pose.value().position);
    const auto point_distance_list =
        lanefollowing::CreateLineFollowDataFromPath(start_to_target_path, start_lane_location, target_lane_location);
    const auto total_distance =
        std::accumulate(point_distance_list.begin(), point_distance_list.end(), 0.0, [&](auto sum, const auto& entry) {
            return sum + entry.distance_to_next_point.value();
        });

    return units::length::meter_t{total_distance};
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LaneLocationProvider::GetPosition(
    const mantle_api::Pose& reference_pose,
    mantle_api::LateralDisplacementDirection direction,
    units::length::meter_t distance) const
{
    const auto reference_pose_lane_location = GetLaneLocation(reference_pose.position);
    if (!reference_pose_lane_location.IsValid())
    {
        Warn("Reference position ({}) is not on a valid lane location.", reference_pose.position);
        return std::nullopt;
    }

    mantle_api::LateralDisplacementDirection direction_relative_to_lane{direction};
    if (!geometry_helper_.AreOrientedSimilarly(reference_pose.orientation,
                                               reference_pose_lane_location.GetOrientation()))
    {
        direction_relative_to_lane = direction == mantle_api::LateralDisplacementDirection::kRight
                                         ? mantle_api::LateralDisplacementDirection::kLeft
                                         : mantle_api::LateralDisplacementDirection::kRight;
    }

    auto corrected_distance =
        (direction_relative_to_lane == mantle_api::LateralDisplacementDirection::kRight) ? distance : -distance;

    const mantle_api::Vec3<units::length::meter_t> normalized_vector = service::glmwrapper::Normalize(
        service::glmwrapper::Cross(reference_pose_lane_location.direction, reference_pose_lane_location.lane_normal));
    const auto lateral_shift = normalized_vector * static_cast<double>(corrected_distance);
    auto target_position = reference_pose.position + lateral_shift;
    auto target_position_lane_location = GetLaneLocation(target_position);

    if (!target_position_lane_location.IsValid() && direction == mantle_api::LateralDisplacementDirection::kAny)
    {
        target_position = reference_pose.position - lateral_shift;
        target_position_lane_location = GetLaneLocation(target_position);
    }

    if (!target_position_lane_location.IsValid())
    {
        Warn("Target position ({}) is not on a valid lane location.", target_position);
        return std::nullopt;
    }
    return target_position;
}

units::length::meter_t LaneLocationProvider::GetLaneHeightAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane_location = GetLaneLocation(position);
    if (!lane_location.IsValid())
    {
        throw EnvironmentException(
            "Unable to get lane altitude for position {},{}. This position is not located withing the road geometry.",
            position.x,
            position.y);
    }

    // 1. Calculate 2d distance of position and projected center line point
    auto position_2d = position;
    position_2d.z = 0_m;
    auto projected_center_line_point_2d = lane_location.projected_centerline_point;
    projected_center_line_point_2d.z = 0_m;
    auto distance = service::glmwrapper::Distance(position_2d, projected_center_line_point_2d);

    // 2. Calculate altitude difference between projected center line point and position
    auto altitude_diff = units::length::meter_t{distance * std::tan(lane_location.GetOrientation().roll())};

    // 3. Invert the sign of the altitude diff if the position is right of the center line (positive roll results in
    // negative altitude diff)
    if (service::glmwrapper::Dot(service::glmwrapper::GetWorldSpaceLeftVector(lane_location.GetOrientation()),
                                 position_2d - projected_center_line_point_2d) < 0)
    {
        altitude_diff *= -1.0;
    }

    return lane_location.projected_centerline_point.z + altitude_diff;
}

}  // namespace gtgen::core::environment::map
