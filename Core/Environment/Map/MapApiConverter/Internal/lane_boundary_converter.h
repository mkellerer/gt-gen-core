/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEBOUNDARYCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEBOUNDARYCONVERTER_H

#include "Core/Environment/Map/GtGenMap/lane_boundary.h"

#include <MapAPI/lane_boundary.h>

namespace gtgen::core::environment::map
{

LaneBoundary ConvertLaneBoundary(const map_api::LaneBoundary& from);

LaneBoundary::Color ConvertLaneBoundaryColor(const map_api::LaneBoundary::Color& from);
LaneBoundary::Type ConvertLaneBoundaryType(const map_api::LaneBoundary::Type& from);
LaneBoundary::Point ConvertBoundaryPoint(const map_api::LaneBoundary::BoundaryPoint& from);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEBOUNDARYCONVERTER_H
