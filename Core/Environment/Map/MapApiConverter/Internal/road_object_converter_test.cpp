/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/road_object_converter.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;

namespace gtgen::core::environment::map::map
{

TEST(ConvertRoadObjectTypeTest, GivenStationaryObjectWithId_WhenConvert_ThenRoadObjectHasSameId)
{
    map_api::StationaryObject stationary_object;
    stationary_object.id = 42;

    const auto result = ConvertRoadObject(stationary_object);

    EXPECT_EQ(result.id, stationary_object.id);
}

TEST(ConvertRoadObjectTypeTest, GivenStationaryObjectWithType_WhenConvert_ThenRoadObjectHasCorrectType)
{
    map_api::StationaryObject stationary_object;
    stationary_object.type = map_api::StationaryObject::Type::kBuilding;

    const auto result = ConvertRoadObject(stationary_object);

    EXPECT_EQ(result.type, mantle_api::StaticObjectType::kBuilding);
}

TEST(ConvertRoadObjectTypeTest,
     GivenStationaryObjectWithPositionOrientationDimension_WhenConvert_ThenRoadObjectHasSamePoseDimension)
{
    map_api::StationaryObject stationary_object;
    stationary_object.base.position = {1.0_m, 2.0_m, 3.0_m};
    stationary_object.base.orientation = {4.0_rad, 5.0_rad, 6.0_rad};
    stationary_object.base.dimension = {7.0_m, 8.0_m, 9.0_m};

    const auto result = ConvertRoadObject(stationary_object);

    EXPECT_EQ(result.pose.position, stationary_object.base.position);
    EXPECT_EQ(result.pose.orientation, stationary_object.base.orientation);
    EXPECT_EQ(result.dimensions, stationary_object.base.dimension);
}

TEST(ConvertRoadObjectTypeTest, GivenStationaryObjectWithBasePolygon_WhenConvert_ThenRoadObjectHasSamBasePolygon)
{
    map_api::StationaryObject stationary_object;
    stationary_object.base.base_polygon = {
        {0.5_m, 1.0_m, 0_m}, {-0.5_m, 1.0_m, 0_m}, {-0.5_m, -1.0_m, 0_m}, {0.5_m, -1.0_m, 0_m}};

    const auto result = ConvertRoadObject(stationary_object);

    EXPECT_EQ(result.base_polygon, stationary_object.base.base_polygon);
}

TEST(ConvertRoadObjectMaterialTest, GivenStationaryObjectWithMaterial_WhenConvert_ThenRoadObjectHasCorrectMaterial)
{
    map_api::StationaryObject stationary_object;
    stationary_object.material = map_api::StationaryObject::Material::kGlas;

    const auto result = ConvertRoadObject(stationary_object);

    EXPECT_EQ(result.material, osi::StationaryObjectEntityMaterial::kGlas);
}

}  // namespace gtgen::core::environment::map::map
