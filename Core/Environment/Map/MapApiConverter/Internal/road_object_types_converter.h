/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTTYPESCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTTYPESCONVERTER_H

#include "Core/Service/Osi/stationary_object_types.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <MapAPI/stationary_object.h>

namespace gtgen::core::environment::map
{
mantle_api::StaticObjectType ConvertRoadObjectType(const map_api::StationaryObject::Type& from);
osi::StationaryObjectEntityMaterial ConvertRoadObjectMaterial(const map_api::StationaryObject::Material& from);
}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTTYPESCONVERTER_H
