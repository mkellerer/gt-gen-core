/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_SUPPLEMENTARYSIGNPROPERTIESPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_SUPPLEMENTARYSIGNPROPERTIESPROVIDER_H

#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <map>
#include <memory>
#include <string>

namespace gtgen::core::environment::static_objects
{

class SupplementarySignPropertiesProvider
{
  public:
    static std::unique_ptr<mantle_api::StaticObjectProperties> Get(const std::string& identifier);

  private:
    static osi::OsiSupplementarySignType GetOsiSupplementarySignType(const std::string& identifier);
    static std::vector<osi::OsiSupplementarySignActor> GetOsiSupplementarySignActors(const std::string& identifier);

    using TypeDataMap = std::map<std::string, osi::OsiSupplementarySignType>;
    const static TypeDataMap kStvoSignsToOsiSupplementarySignTypes;

    using ActorDataMap = std::map<std::string, std::vector<osi::OsiSupplementarySignActor>>;
    const static ActorDataMap kStvoSignsToOsiSupplementarySignActors;

    static std::unique_ptr<mantle_api::StaticObjectProperties> ToProperties(const std::string& identifier);
};

}  // namespace gtgen::core::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_SUPPLEMENTARYSIGNPROPERTIESPROVIDER_H
