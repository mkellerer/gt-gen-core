/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/file_system_utils.h"

#include "Core/Service/Exception/exception.h"

#include <gtest/gtest.h>
#include <sys/stat.h>

#include <fstream>

namespace gtgen::core::service::file_system
{

namespace
{

const auto TEST_DATA_DIR_PATH = fs::path{fs::absolute(__FILE__)}.parent_path();

struct FileComparisonParams
{
    std::string expected_file;
    std::string actual_file;
    std::string expected_content;
    bool expected_result;
};

class AreFilesEqualTest : public ::testing::TestWithParam<FileComparisonParams>
{
  protected:
    void SetUp() override
    {
        auto param = GetParam();
        CreateFileWithContent(TEST_DATA_DIR_PATH / param.expected_file, param.expected_content);
    }

    void CreateFileWithContent(const fs::path& file_path, const std::string& content)
    {
        std::ofstream file(file_path);

        IsFileOpen(file, file_path);

        file << content;

        file.close();
    }
};

}  // namespace

TEST(FileSystemUtilityTest, GivenNonExistingDirectory_WhenCreateDirectory_ThenDirectoryIsCreated)
{
    auto empty_folder =
        CreateOrWipeDirectoryToEnsureEmpty(fs::temp_directory_path() / "GtGenCoreTests", "FileSystemUtilityTest");
    fs::path to_be_created(empty_folder / "to_be_created");

    EXPECT_TRUE(FileExists(empty_folder));
    EXPECT_TRUE(fs::is_empty(empty_folder));
    EXPECT_FALSE(FileExists(to_be_created));

    CreateDirectoryIfNotExisting(to_be_created);
    EXPECT_TRUE(FileExists(to_be_created));
}

TEST(FileSystemUtilityTest, GivenHomeDirectoryExists_WhenGetHomeDirectory_ThenPathExists)
{
    auto path = GetHomeDirectory();
    EXPECT_TRUE(fs::exists(path));
}

TEST(IsNdsMapTest, GivenNdsMapPath_WhenIsNdsMap_ThenReturnTrue)
{
    EXPECT_TRUE(IsNdsMap(fs::path{"/some/path/to/a/large/map/ROOT.NDS"}));
    EXPECT_TRUE(IsNdsMap(fs::path{"/some/path/to/a/large/map/lowercase_name.nds"}));
    EXPECT_TRUE(IsNdsMap(fs::path{"very_short.nds"}));
    EXPECT_TRUE(IsNdsMap(fs::path{"ROOT.NDS"}));
}

TEST(IsNdsMapTest, GivenNonNdsMapPath_WhenIsNdsMap_ThenReturnFalse)
{
    EXPECT_FALSE(IsNdsMap(fs::path{"/some/path/to/a/largemap/map.xodr"}));
    EXPECT_FALSE(IsNdsMap(fs::path{"/some/path/to/a/large/map/lowercase_name.ndsx"}));
    EXPECT_FALSE(IsNdsMap(fs::path{"very_short.NDSX"}));
    EXPECT_FALSE(IsNdsMap(fs::path{"ROOT"}));
}

TEST(IsOdrMapTest, GivenOdrMap_WhenIsOdrMap_ThenReturnTrue)
{
    EXPECT_TRUE(IsOdrMap(fs::path{"/some/path/to/a/large/map/simple.xodr"}));
    EXPECT_TRUE(IsOdrMap(fs::path{"very_short.xodr"}));
}

TEST(IsOdrMapTest, GivenNonOdrMap_WhenIsOdrMap_ThenReturnFalse)
{
    EXPECT_FALSE(IsOdrMap(fs::path{"/some/path/to/a/large/map/lowercase_name.odr"}));
    EXPECT_FALSE(IsOdrMap(fs::path{"very_short.odr"}));
    EXPECT_FALSE(IsOdrMap(fs::path{"simple"}));
}

TEST(GetFileExtensionTest, GivenPathWithFileAndExtension_WhenGetFileExtension_ThenFileExtensionReturned)
{
    EXPECT_EQ("nds", GetFileExtension("/path/to/some/map/ROOT.NDS"));
    EXPECT_EQ("xodr", GetFileExtension("map.xodr"));
    EXPECT_EQ("ini", GetFileExtension("foo/UserSettings.ini"));
}

TEST(GetFileExtensionTest, GivenPathWithoutFileAOrExtension_WhenGetFileExtension_ThenEmptyStringReturned)
{
    EXPECT_EQ("", GetFileExtension("file_without_extension"));
    EXPECT_EQ("", GetFileExtension(""));
    EXPECT_EQ("", GetFileExtension("/absolute/path/"));
    EXPECT_EQ("", GetFileExtension("/absolute/path/without_ending_slash"));
    EXPECT_EQ("", GetFileExtension("/just/dot/."));
    EXPECT_EQ("", GetFileExtension("."));
}

TEST(CreateEmptyFileTest, GivenDirectoryWithNoFile_WhenCreateEmptyFile_ThenFileExists)
{
    auto dir = CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "bar");
    fs::path file(dir / "foo.txt");
    ASSERT_FALSE(FileExists(file));

    CreateEmptyFile(file);

    EXPECT_TRUE(FileExists(file));
}

TEST(CreateFileTest, GivenDirectoryWithNoFile_WhenCreateFile_ThenFileExistsAndContentCorrect)
{
    std::string expected_content("text");
    auto dir = CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "bar");
    fs::path file(dir / "foo.txt");
    ASSERT_FALSE(FileExists(file));

    CreateFile(file, expected_content);

    ASSERT_TRUE(FileExists(file));

    std::ifstream read_stream(file.string());
    std::stringstream buffer;
    buffer << read_stream.rdbuf();

    EXPECT_EQ(expected_content, buffer.str());
}

TEST(SearchRecursivelyInDirectory, GivenSearchSameAsDirectory_WhenSearchRecursivelyInDirectory_ThenImmendiatellyFound)
{
    std::string file_name("without_recursion_found_file.txt");
    CreateEmptyFile(fs::current_path() / file_name);
    auto path = SearchRecursivelyInDirectory(file_name, fs::current_path());
    EXPECT_EQ(path, fs::current_path() / file_name);
}

TEST(SearchRecursivelyInDirectory, GivenInvalidTopLevelDirectory_WhenSearchRecursivelyInDirectory_ThenThrow)
{
    EXPECT_THROW(SearchRecursivelyInDirectory("blub", "non/existing/toplevel/dir"), ServiceException);
}

TEST(SearchRecursivelyInDirectory,
     GivenDirectoryWithSubDirsAndOneFile_WhenSearchingRecursivelyForTheFile_ThenFileIsFound)
{
    fs::path root = fs::current_path();
    auto subdir1 = CreateOrWipeDirectoryToEnsureEmpty(root, "subdir1");
    CreateOrWipeDirectoryToEnsureEmpty(root, "subdir2");
    auto folder_with_file = CreateOrWipeDirectoryToEnsureEmpty(subdir1, "folder_with_file");
    std::string file_name{"empty.txt"};

    auto expected_path = fs::canonical(folder_with_file) / file_name;
    CreateEmptyFile(expected_path);

    auto found_file_path = SearchRecursivelyInDirectory(file_name, root);

    EXPECT_EQ(found_file_path, expected_path);
}

TEST(SearchRecursivelyInDirectory,
     GivenDirectoryWithSubDirsAndNoFiles_WhenSearchingRecursivelyForAFile_ThenNothingIsFound)
{
    fs::path root = fs::current_path();
    auto subdir1 = CreateOrWipeDirectoryToEnsureEmpty(root, "subdir1");
    CreateOrWipeDirectoryToEnsureEmpty(root, "subdir2");
    auto subdir2 = CreateOrWipeDirectoryToEnsureEmpty(subdir1, "subdir2");

    auto found_file_path = SearchRecursivelyInDirectory("not-existing-file.txt", root);

    EXPECT_TRUE(found_file_path.empty());
}

TEST(ReplaceTildeWithHomeDirTest, GivenPathWithoutTilde_WhenReplacing_ThenPathDoesNotChange)
{
    fs::path expected_path{"./directory/file.txt"};
    fs::path path{expected_path};

    ReplaceTildeWithAbsoluteHomeDirectoryPath(path);

    EXPECT_EQ(expected_path.string(), path.string());
}

TEST(ReplaceTildeWithHomeDirTest, GivenPathWithTilde_WhenReplacing_ThenPathDoesChange)
{
    fs::path expected_path{"~/directory/file.txt"};
    fs::path path{expected_path};

    ReplaceTildeWithAbsoluteHomeDirectoryPath(path);

    EXPECT_NE(expected_path.string(), path.string());
    EXPECT_NE("~", path.begin()->string());
}

TEST(SetRequiredReadWritePermissionsTest, GivenReadOnlyFile_WhenSetRequiredReadWritePermissions_ThenAlsoWriteable)
{
    fs::path read_only = fs::current_path() / "read_only.txt";
    CreateEmptyFile(read_only);
    fs::permissions(read_only, fs::perms::owner_write | fs::perms::remove_perms);
    ASSERT_EQ(fs::status(read_only).permissions() & fs::perms::owner_write, fs::perms::none);

    SetRequiredReadWritePermissions(read_only);

    EXPECT_EQ(fs::status(read_only).permissions() & fs::perms::owner_write, fs::perms::owner_write);
}

TEST(DetermineBackupFileNameTest, GivenExistingFileToBackup_WhenDetermineBackupFileName_ThenBackupExtensionIsBak)
{
    auto folder = CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "DetermineBackupFileNameTest");
    fs::path input_file = folder / "input_file.txt";
    CreateEmptyFile(input_file);

    auto backup = DetermineBackupFileName(input_file);

    EXPECT_EQ(backup.string(), fs::path(folder / "input_file.txt.bak").string());
}

TEST(DetermineBackupFileNameTest, GivenOneExistingBackup_WhenDetermineBackupFileName_ThenBackupExtensionIsBak1)
{
    auto folder = CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "DetermineBackupFileNameTest");
    fs::path input_file = folder / "input_file.txt";
    fs::path existing_backup = folder / "input_file.txt.bak";
    CreateEmptyFile(input_file);
    CreateEmptyFile(existing_backup);

    auto backup = DetermineBackupFileName(input_file);

    EXPECT_EQ(backup.string(), fs::path(folder / "input_file.txt.bak1").string());
}

TEST(DetermineBackupFileNameTest, GivenTwoExistingBackups_WhenDetermineBackupFileName_ThenBackupExtensionIsBak2)
{
    auto folder = CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "DetermineBackupFileNameTest");
    fs::path input_file = folder / "input_file.txt";
    fs::path existing_backup = folder / "input_file.txt.bak";
    fs::path existing_backup1 = folder / "input_file.txt.bak1";
    CreateEmptyFile(input_file);
    CreateEmptyFile(existing_backup);
    CreateEmptyFile(existing_backup1);

    auto backup = DetermineBackupFileName(input_file);

    EXPECT_EQ(backup.string(), fs::path(folder / "input_file.txt.bak2").string());
}

TEST(IsFileOpenTest, GivenFileThatCantBeOpen_WhenOpeningFile_ThenExpectThrow)
{
    fs::path dummy_path("/path/that/do/not/exist/file");

    std::ifstream stream{dummy_path};

    ASSERT_ANY_THROW(IsFileOpen(stream, dummy_path));
}

TEST(IsFileOpenTest, GivenFileThatCanBeOpen_WhenOpeningFile_ThenFileOpenedCorrectly)
{
    // We take this file as a sample since we know it exists
    auto file_path = fs::path{test_info_->file()}.parent_path() / "test_check_equality.txt";

    std::ifstream stream{file_path};

    ASSERT_NO_THROW(IsFileOpen(stream, file_path));
}

TEST_P(AreFilesEqualTest, GivenTwoFiles_WhenCheckingEquality_ThenEqualityAsExpected)
{
    auto parameters = GetParam();
    EXPECT_EQ(AreFilesEqual(TEST_DATA_DIR_PATH / parameters.expected_file, TEST_DATA_DIR_PATH / parameters.actual_file),
              parameters.expected_result);
}

INSTANTIATE_TEST_SUITE_P(FilesComparisons,
                         AreFilesEqualTest,
                         ::testing::Values(FileComparisonParams{"file_created.txt",
                                                                "test_check_inequality.txt",
                                                                "dummy text to check file equality\n",
                                                                false},
                                           FileComparisonParams{"file_created.txt",
                                                                "test_check_equality.txt",
                                                                "dummy text to check file equality\n",
                                                                true}));

}  // namespace gtgen::core::service::file_system
