/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_UTILS_H
#define GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_UTILS_H

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Version/version.h"
#include "fmt/core.h"

#include <string>

namespace gtgen::core::service
{

const fs::path GenerateDefaultTracePath(const std::string scenario)
{
    const auto osi_version_str = std::to_string(GetOsiVersion().major) + "_" + std::to_string(GetOsiVersion().minor) +
                                 "_" + std::to_string(GetOsiVersion().patch);

    const auto gtgen_core_version_str = std::to_string(gtgen::core::gtgen_core_version.major) + "_" +
                                        std::to_string(gtgen::core::gtgen_core_version.minor) + "_" +
                                        std::to_string(gtgen::core::gtgen_core_version.patch);

    return fs::temp_directory_path() /
           fmt::format(
               "SensorView_Scenario_{}_OSI_{}_GTGEN_CORE_{}.osi", scenario, osi_version_str, gtgen_core_version_str);
}

}  // namespace gtgen::core::service

#endif  // GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_UTILS_H
