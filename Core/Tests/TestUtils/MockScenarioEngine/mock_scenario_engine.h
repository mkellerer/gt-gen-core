/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MOCKSCENARIOENGINE_MOCKSCENARIOENGINE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MOCKSCENARIOENGINE_MOCKSCENARIOENGINE_H

#include <gmock/gmock.h>

namespace gtgen::core::test_utils
{

class MockScenarioEngine : public mantle_api::IScenarioEngine
{
  public:
    MOCK_METHOD(void, Init, (), (override));
    MOCK_METHOD(void, SetupDynamicContent, (), (override));
    MOCK_METHOD(mantle_api::ScenarioInfo, GetScenarioInfo, (), (const));
    MOCK_METHOD(void, Step, (mantle_api::Time), (override));
    MOCK_METHOD(bool, IsFinished, (), (const, override));
    MOCK_METHOD(void, ActivateExternalHostControl, (), (override));
    MOCK_METHOD(int, ValidateScenario, (), (override));
    MOCK_METHOD(std::optional<mantle_api::Time>, GetDesiredDeltaTime, (), (const, noexcept));
};

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MOCKSCENARIOENGINE_MOCKSCENARIOENGINE_H
