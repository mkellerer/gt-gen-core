load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "8.0.1"

def fmt():
    maybe(
        http_archive,
        name = "fmt",
        sha256 = "b06ca3130158c625848f3fb7418f235155a4d389b2abc3a6245fb01cb0eb1e01",
        url = "https://github.com/fmtlib/fmt/archive/refs/tags/{version}.tar.gz".format(version = _VERSION),
        build_file = Label("//:third_party/fmt/fmt.BUILD"),
        strip_prefix = "fmt-{version}".format(version = _VERSION),
    )
