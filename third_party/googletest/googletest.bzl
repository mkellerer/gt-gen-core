load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "1.12.1"

def googletest():
    maybe(
        http_archive,
        name = "googletest",
        url = "https://github.com/google/googletest/archive/refs/tags/release-{}.tar.gz".format(_VERSION),
        sha256 = "81964fe578e9bd7c94dfdb09c8e4d6e6759e19967e397dbea48d1c10e45d0df2",
        strip_prefix = "googletest-release-{}".format(_VERSION),
        patches = [
            # Gtest >= 1.11.0 is impacted by a GCC compiler bug regarding template deduction.
            # See: https://github.com/google/googletest/issues/3552
            # We backport the upstream fix to this until we reach a gtest version incorporating it by itself.
            Label("//:third_party/googletest/gcc_printer_patch.patch"),
        ],
        patch_args = ["-p1"],
    )
