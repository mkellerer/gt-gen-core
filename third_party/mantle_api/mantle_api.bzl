load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")

_TAG = "v11.0.0"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "a723c0815fd524fa932bedcf8e9132cbcde8e4788c1ab8b59bfb7abcfbd4799f",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
