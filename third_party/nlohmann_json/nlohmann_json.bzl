load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "3.9.1"

def nlohmann_json():
    maybe(
        http_archive,
        name = "nlohmann_json",
        build_file = Label("//:third_party/nlohmann_json/nlohmann_json.BUILD"),
        url =  "https://github.com/nlohmann/json/releases/download/v{version}/include.zip".format(version = _VERSION),
        sha256 = "6bea5877b1541d353bd77bdfbdb2696333ae5ed8f9e8cc22df657192218cad91",
    )
