"""
This module contains rule to pull openssl library from ubuntu archives.
"""

load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@gt_gen_core//bazel/rules:artifactory_deb.bzl", "artifactory_deb_group")

_NAME = "openssl"
_VERSION = "3.0.13-0ubuntu3.5_amd64"
_COPYRIGHT = "Copyright (C) 1999-2025, OpenSSL Project Authors."

def openssl():
    maybe(
        artifactory_deb_group,
        name = "openssl",
        build_file = Label("//:third_party/openssl/openssl.BUILD"),
        repo = "http://archive.ubuntu.com/ubuntu",
        package_group = {
            "pool/main/o/openssl/libssl-dev_{version}.deb".format(version = _VERSION): "e481a6c9e1737a31940891b7953bf3b5c70baa5cb1ad2751d892da904b9ff52d",
            "pool/main/o/openssl/libssl3t64_{version}.deb".format(version = _VERSION): "31df0a4d957c404c01b1898a84beca1190fd71cf2d2ac47ce5036705a24d96fc",
        },
    )
