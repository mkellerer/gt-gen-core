load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "26.1"

def protobuf():
    maybe(
        http_archive,
        name = "com_google_protobuf",
        sha256 = "4fc5ff1b2c339fb86cd3a25f0b5311478ab081e65ad258c6789359cd84d421f8",
        strip_prefix = "protobuf-{version}".format(version = _VERSION),
        url = "https://github.com/protocolbuffers/protobuf/archive/v{version}.tar.gz".format(version = _VERSION),
    )
