load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "0.5"


def tracy_profiler():
    maybe(
        http_archive,
        name = "tracy_profiler",
        build_file = Label("//:third_party/tracy/tracy.BUILD"),
        sha256 = "72d5385bcebde20d516ff451e3d2974e0f4db1995259cf2737ce5233844cae53",
        strip_prefix = "afrasson-tracy-ca693f2b280c",
        url = "https://github.com/wolfpld/tracy/archive/refs/tags/v{version}.tar.gz".format(version = _VERSION),
        patches = [Label("//:third_party/tracy/tracy.patch")],
        patch_args = ["-p1"],
    )
